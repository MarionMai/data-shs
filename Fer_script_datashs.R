###### Représenter les échanges internationaux de ferraille avec VisNetwork

# charger les packages

library("igraph")
library('visNetwork')

# ouvrir le fichier contenant la liste de liens

flux <- read.csv("data_iron_waste_2007_comwaste.csv")

# le transformer en graphe

g<-graph.data.frame(flux[,1:2], directed = TRUE)

# créer un attribut "poids" sur les liens 

E(g)$weight<-flux[,3]

# représenter le graphe

plot(g)

--------------------------------------------------------------

## ne conserver que la composante principale

# afficher la liste des composantes

clusters(g)

# réduire le graphe à sa composante principale

g<-induced.subgraph(g, clusters(g)$membership==1)

# vérifier qu'il ne reste que la composante principale
# afficher la liste des composantes

clusters(g)

---------------------------------------------------------------

# transformer le graphe en object VisNetwork

data <- toVisNetworkData(g)

# visualiser

visNetwork(nodes = data$nodes, edges = data$edges)

------------------------------------------------------------------
### paramétrer l'apparence des sommets et des liens

## consulter l'aide pour consulter les options de visualisation
#?visNodes
#?visEdges

## paramétrer l'apparence des sommets

data$nodes$size<-degree(g)                # taille du sommet
data$nodes$shape <- "dot"  
data$nodes$shadow <- TRUE                 # ajouter une ombre
data$nodes$title <- data$nodes$label      # le label apparait au clic
data$nodes$borderWidth <- 1               # épaisseur du contour des sommets
data$nodes$color.background <- "orange"
data$nodes$color.border <- "darkgrey"
data$nodes$color.highlight.background <- "darkred"
data$nodes$color.highlight.border <- "darkred"

## paramétrer l'apparence des liens

# fixer la largeur du plus gros flux

maxsize<-10 # epaisseur du plus gros lien

# paramétrer l'épaisseur des liens

data$edges$width<-(((data$edges$weight/1000000)/max((data$edges$weight/1000000)))*maxsize) 

# autres paramètres sur les liens

data$edges$color <- "gray"    			# couleur 
data$edges$arrows <- "middle" 			# flèches: 'from', 'to', ou 'middle'
data$edges$smooth <- FALSE    			# courbure du lien
data$edges$shadow <- FALSE    			# ombre du lien

----------------------------------------------------------------------------------------

# visualiser

visNetwork(nodes = data$nodes, edges = data$edges)

----------------------------------------------------------------------------------------
# visualiser en ajoutant un menu déroulant pour filtrer

visNetwork(nodes = data$nodes, edges = data$edges) %>%
  visOptions(highlightNearest = TRUE, 
             selectedBy = "label")


