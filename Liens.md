# Liste de liens pointant vers des publications et autres supports utiles pour la séance


## Analyse de réseaux

- Carnet de recherche du groupe FMR : [https://groupefmr.hypotheses.org/](https://groupefmr.hypotheses.org/)
  - Notamment le tutoriel d'introduction aux réseaux dans R de L. Beauguitte : [https://groupefmr.hypotheses.org/1765](https://groupefmr.hypotheses.org/1765)
- Carnet de recherche du Groupement AR-SHS : [https://arshs.hypotheses.org/403](https://arshs.hypotheses.org/403)
- Synthèses du groupe FMR : [https://halshs.archives-ouvertes.fr/FMR/](https://halshs.archives-ouvertes.fr/FMR/)
- Réseaux avec R, Blog de Lise Vaudor (IR CNRS, UMR EVS), R-atique : [http://perso.ens-lyon.fr/lise.vaudor/](http://perso.ens-lyon.fr/lise.vaudor/)


## Sémiologie graphique

- Cours d'introduction de L. Jégou : [http://www.geotests.net/cours/202/UE202_s6_semio_2019.pdf](http://www.geotests.net/cours/202/UE202_s6_semio_2019.pdf)
- MOOC plus général sur les données en géographie : [http://goo.gl/s56JkN](http://goo.gl/s56JkN)
- Ouvrage de N. Lambert et Ch. Zanin : [https://www.armand-colin.com/manuel-de-cartographie-principes-methodes-applications-9782200612856](https://www.armand-colin.com/manuel-de-cartographie-principes-methodes-applications-9782200612856)
- Ouvrage de Ch. Zanin et M-L Trémélo : [https://www.belin-education.com/savoir-faire-une-carte](https://www.belin-education.com/savoir-faire-une-carte)


## Tutoriels pour produire des cartes de flux et des diagrammes noeuds-liens

* Supports de la séance sur la visualisation des réseaux GDR AR-SHS - école de Nice 2017 et 2019 : [https://framagit.org/MarionMai/Visualisation-de-reseaux](https://framagit.org/MarionMai/Visualisation-de-reseaux)
* Faire une carte de flux avec QGIS en utilisant le package Network_Analysis : [https://groupefmr.hypotheses.org/4742](https://groupefmr.hypotheses.org/4742)
* [Créer des cartes de flux avec une légende sous R à l'aide du package _cartography_ par Timothée Giraud et Nicolas Lambert](https://cran.r-project.org/web/packages/cartography/vignettes/cartography.html#linkflow-map)
* [Créer une carte de flux avec le package R _ggplot_ par Peter Prevos](http://r.prevos.net/create-air-travel-route-maps/)
* [Le projet github du package _ggraph_ pour la visualisation de réseaux sous R. _Grammar of Graph Graphics_](https://github.com/thomasp85/ggraph)
* [L'incontournable tutoriel de Katya Ognyanova. _Network Visualization with R_](http://kateto.net/network-visualization)
* [Sa traduction en français par Laurent Beauguitte. La partie consacrée à la visualisation statique](https://f.hypotheses.org/wp-content/blogs.dir/2996/files/2017/02/visualiseR.pdf)
* [Sa traduction en français par Laurent Beauguitte. La partie consacrée à la visualisation dynamique](https://f.hypotheses.org/wp-content/blogs.dir/2996/files/2017/02/visualiseR_2.pdf)
* [Présentation et code de James Curley. _Interactive and Dynamic Network Visualization in R_](http://curleylab.psych.columbia.edu/netviz/)
* [_GraphViz_ et _DiagrammeR_. Petit tuto pour visualiser des diagrammes avec R](https://blog.rstudio.org/2015/05/01/rstudio-v0-99-preview-graphviz-and-diagrammer/)
* [Introduction à la visualisation de données : l’analyse de réseau en histoire. Par Martin Grandjean](http://www.martingrandjean.ch/introduction-visualisation-de-donnees-analyse-de-reseau-histoire/)
* [La visualisation de données en sciences humaines est un moyen, pas une fin ! Par Martin Grandjean](http://www.martingrandjean.ch/archive-reseau-visualisation-donnes-sciences-humaines/)

## Exemples de représentations graphiques web et de données disponibles

* CoSciMo, les collaborations scientifiques mondiales : [http://coscimo.net/](http://coscimo.net/)
* Manuel Lima, projet Visual Complexity : [http://www.visualcomplexity.com/vc/](http://www.visualcomplexity.com/vc/)
* Andy Kirk, Visualizing Data, une curation mensuelle du web : [https://www.visualisingdata.com/](https://www.visualisingdata.com/)
* [La liste de ressources proposées par Katya Ognyanova](http://kateto.net/2016/05/network-datasets/)
* [La liste de ressources proposées par François Briatte](https://github.com/briatte/awesome-network-analysis#datasets)
* [Galerie de représentations issues de l'ouvrage _Visual Complexity_ de Manuel Lima](http://www.visualcomplexity.com/vc/)
* [_Visualizing Historical Networks_ par le Centre d'Histoire et d'Economie de l'Université d'Harvard](https://www.fas.harvard.edu/%7Ehistecon/visualizing/index.html)
* [Le projet Palladio par le centre d'Humanité Numérique de l'Université de Stanford](http://hdlab.stanford.edu/palladio/)
* [Poster: _The Historic Development of Network Visualization_ par Jürgen Pfeffer et Lin Freeman](http://www.pfeffer.at/data/visposter/)

## Outils et applications

- Arabesque, prototype du groupe gFlowiz : [http://arabesque.ifsttar.fr/](http://arabesque.ifsttar.fr/)  
  - son tutoriel pour les données RICARDO : [https://github.com/gflowiz/sageo-ricardo](https://github.com/gflowiz/sageo-ricardo)
- Prototype NetMap : [https://geotests.net/net_map/fer.html](https://geotests.net/net_map/fer.html)
  - Code source ouvert : [https://gitlab.com/ljegou/netmap](https://gitlab.com/ljegou/netmap)
- Application ComWaste : [http://www.geotests.net/comwaste/](http://www.geotests.net/comwaste/)
- Application NETSCITY pour cartographier la production scientifique : [https://www.irit.fr/netscity](https://www.irit.fr/netscity)
  - L'article de présentation de NETSCITY lors de la conférence ISSI'19 : [https://www.irit.fr/publis/IRIS/2019_ISSI_MJYC.pdf](https://www.irit.fr/publis/IRIS/2019_ISSI_MJYC.pdf)
- Application The Vistorian adadptée aux données relationnelles multiplexes : [https://vistorian.wordpress.com/](https://vistorian.wordpress.com/)
- Logiciels d'analyse et de visualisation de réseau :
  - Tulip : [http://tulip.labri.fr/TulipDrupal/](http://tulip.labri.fr/TulipDrupal/)
  - Cytoscape : [http://www.cytoscape.org/](http://www.cytoscape.org/)
  - SocNetV : [http://socnetv.org/](http://socnetv.org/)
  - Ucinet : [https://sites.google.com/site/ucinetsoftware/](https://sites.google.com/site/ucinetsoftware/)
  - Pajek : [http://mrvar.fdv.uni-lj.si/pajek/](http://mrvar.fdv.uni-lj.si/pajek/) Tutoriel : [https://quanti.hypotheses.org/512/](https://quanti.hypotheses.org/512/)
  - Gephi : https://gephi.org/ Tutoriels et ressources : [https://seinecle.github.io/gephi-tutorials/](https://seinecle.github.io/gephi-tutorials/) et [http://www.martingrandjean.ch/gephi-introduction/](http://www.martingrandjean.ch/gephi-introduction/)
- Pour aller plus loin, une liste : [https://fr.wikipedia.org/wiki/Analyse_des_r%C3%A9seaux_sociaux](https://fr.wikipedia.org/wiki/Analyse_des_r%C3%A9seaux_sociaux)