# DATA-SHS

_Introduction to the exploration and representation of flows on a global scale: the example of international trade and scientific collaboration between cities_

Ce projet contient les documents, liens et supports préparés pour la formation proposée lors de la semaine DATA-SHS organisée par la MSH de Toulouse séance finale du vendredi 13 décembre 2019.

_Titre_ : Introduction à l’exploration et la représentation de flux à l’échelle mondiale : l’exemple du commerce international et des collaborations scientifiques entre villes

_Intervenants_ : Laurent Jégou - MCF en géographie (UT2J, LISST, équipe CIEU, UMR 5193) & Marion Maisonobe - Chargée de Recherche équipe P.A.R.I.S. (CNRS, Géographie-Cités, UMR 8504) 

_Prérequis_ : aucun. Installer R sur son ordinateur portable : [https://rstudio.com/products/rstudio/download/#download](https://rstudio.com/products/rstudio/download/#download) 

_Résumé_ : Cet atelier vise à initier les participants à la visualisation interactive de données relationnelles. Des notions de base en analyse de réseaux, en cartographie automatique et en sémiologie graphique y seront introduites. Nous prendrons l’exemple de programmes de recherches en cours sur les flux internationaux de déchets et sur les collaborations scientifiques mondiales. Après la présentation des notions de base, nous ferons la démonstration de deux plateformes web : la plateforme Arabesque pour cartographier des flux et la plateforme Netscity pour traiter des données issues de l’activité scientifique. Dans une seconde partie, les utilisateurs visualiseront des données de flux à l’aide du package R ‘VisNetwork’. Nous finirons par une présentation du prototype d’exploration visuelle NETMAP qui s’appuie sur la version web de VisNetwork et nous partagerons des ressources utiles pour approfondir ces méthodes et techniques.
